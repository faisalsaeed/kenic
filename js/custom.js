
// Custom JS 

/*
* @author: Faisal Saeed
*/
$(function SieveOfEratosthenesCached(n, cache) {
  $(document).ready(function () {


    $(document).click(function (e) {
      if (!$(e.target).is('a')) {
        $('.collapse').collapse('hide');
      }
    });


    $('[data-toggle="popover"]').popover({
      trigger: 'focus',
      trigger: 'hover'
    });

    /*scroll to top*/
    $(window).scroll(function () {
      if ($(this).scrollTop() > 100) {
        $('.scrollup').fadeIn();
      } else {
        $('.scrollup').fadeOut();
      }
    });
    $('.scrollup').click(function () {
      $("html, body").animate({
        scrollTop: 0
      }, 600);
      return false;
    });

    /*add class in top logn form*/
    $(window).on("scroll", function () {
      if ($(window).scrollTop() >= 500) {
        $("header").addClass("header-scroll");
      } else {
        $("header").removeClass("header-scroll");
      }
    });

    /*overlay search*/
    $('a[href="#search"]').on("click", function (event) {
      event.preventDefault();
      $("#search").addClass("open-search");
      $('#search > form > input[type="search"]').focus();
    });

    $("#search, #search button.close-search").on("click keyup", function (event) {
      if (
        event.target == this ||
        event.target.className == "close-search" ||
        event.keyCode == 27
      ) {
        $(this).removeClass("open-search");
      }
    });


    /*nice select initializer*/
    $(function () {
      $('select').niceSelect();
    });


    /*tab scroll down*/
    $('a.nav-link').on('shown.bs.tab', function (e) {
      var href = $(this).attr('href');
      $('html, body').animate({
        scrollTop: $(href).offset().top - 100
      }, 'slow');
    });

  });
});

function myFunction(btn_id) {
  var btnText = document.getElementById(btn_id.id);
  var parentElem = btn_id.parentElement;
  var childElemP = parentElem.childNodes;
  var spanElem = childElemP[1].childNodes;

  if (spanElem[1].style.display === "none") {
    spanElem[1].style.display = "inline";
    btnText.innerHTML = "Read more";
    spanElem[2].style.display = "none";
  } else {
    spanElem[1].style.display = "none";
    btnText.innerHTML = "Read less";
    spanElem[2].style.display = "inline";
  }
}









